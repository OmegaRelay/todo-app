import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todo_app/src/providers/todo_provider.dart';
import 'package:todo_app/src/screens/todo_screen.dart';
import 'package:todo_app/src/screens/done_screen.dart';

class AppScreen extends StatelessWidget {
  const AppScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        appBar: _appBar(context),
        body: ChangeNotifierProvider(
          create: (context) => TodoProvider(),
          child:
              const TabBarView(children: <Widget>[TodoScreen(), DoneScreen()]),
        ),
      ),
    );
  }

  AppBar _appBar(BuildContext context) {
    return AppBar(
      title: const Text('Todo List'),
      centerTitle: true,
      foregroundColor: Theme.of(context).colorScheme.onPrimary,
      backgroundColor: Theme.of(context).colorScheme.primary,
      bottom: TabBar(
        tabs: <Widget>[
          Tab(
            icon: Icon(
              Icons.list,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
          ),
          Tab(
            icon: Icon(Icons.check,
                color: Theme.of(context).colorScheme.onPrimary),
          ),
        ],
      ),
    );
  }
}
