import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/src/providers/todo_provider.dart';

class TodoScreen extends StatelessWidget {
  const TodoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TodoProvider todoState = Provider.of<TodoProvider>(context);
    Widget view;

    if (todoState.activeTodoList.isEmpty) {
      view = _noTaskView(context);
    } else {
      view = _taskListView(context);
    }

    return Scaffold(
      body: view,
      floatingActionButton: _addTodoButton(context),
    );
  }

  Widget _noTaskView(BuildContext context) {
    return Center(
        child: Text(
      "There are no tasks left \nAdd todo tasks using the button",
      textAlign: TextAlign.center,
      style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
    ));
  }

  Widget _taskListView(BuildContext context) {
    TodoProvider todoState = Provider.of<TodoProvider>(context);
    return ListView.builder(
      itemCount: todoState.activeTodoList.length,
      itemBuilder: (BuildContext context, int index) {
        TodoProvider todoState = Provider.of<TodoProvider>(context);
        return ListTile(
            title: Text(todoState.activeTodoList[index].description),
            trailing: IconButton(
              icon: const Icon(Icons.check),
              color: Colors.green,
              onPressed: () {
                todoState.clearTodo(index);
              },
            ));
      },
    );
  }

  Widget _addTodoButton(BuildContext context) {
    TodoProvider todoState = Provider.of<TodoProvider>(context);
    return FloatingActionButton(
      onPressed: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              var controller = TextEditingController();
              return AlertDialog(
                title: const Text("Add Todo"),
                content: TextField(
                  autocorrect: true,
                  autofocus: true,
                  decoration:
                      const InputDecoration(hintText: "Enter Description"),
                  controller: controller,
                ),
                actions: [
                  TextButton(
                      onPressed: () {
                        if (controller.text.isNotEmpty) {
                          todoState.addTodo(controller.text);
                        }
                        Navigator.of(context).pop();
                      },
                      child: const Text("SUBMIT"))
                ],
              );
            });
      },
      foregroundColor: Theme.of(context).colorScheme.onPrimary,
      backgroundColor: Theme.of(context).colorScheme.primary,
      shape: const CircleBorder(),
      child: const Icon(Icons.add_task_rounded),
    );
  }
}
