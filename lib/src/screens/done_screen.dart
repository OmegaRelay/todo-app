import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/src/providers/todo_provider.dart';

class DoneScreen extends StatelessWidget {
  const DoneScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TodoProvider todoState = Provider.of<TodoProvider>(context);
    Widget view;

    if (todoState.inactiveTodoList.isEmpty) {
      view = _noTaskView();
    } else {
      view = _taskListView(context);
    }

    return Scaffold(body: view);
  }

  Widget _noTaskView() {
    return const Center(child: Text("There are no completed tasks"));
  }

  Widget _taskListView(BuildContext context) {
    TodoProvider todoState = Provider.of<TodoProvider>(context);
    return ListView.builder(
      itemCount: todoState.inactiveTodoList.length,
      itemBuilder: (BuildContext context, int index) {
        TodoProvider todoState = Provider.of<TodoProvider>(context);
        return ListTile(
          title: Text(todoState.inactiveTodoList[index].description),
          trailing: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
            IconButton(
              icon: const Icon(Icons.settings_backup_restore),
              color: Colors.green,
              onPressed: () {
                todoState.restoreTodo(index);
              },
            ),
            IconButton(
              icon: const Icon(Icons.delete),
              color: Colors.red,
              onPressed: () {
                todoState.removeTodo(index);
              },
            )
          ]),
        );
      },
    );
  }
}
