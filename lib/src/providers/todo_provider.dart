import 'package:flutter/material.dart';
import 'package:todo_app/src/models/todo_model.dart';

class TodoProvider extends ChangeNotifier {
  // TodoModel initTodo = TodoModel(0, "hello World");
  List<TodoModel> activeTodoList = [];
  List<TodoModel> inactiveTodoList = [];

  TodoProvider() {}

  /// Add a todo item to the todo list
  /// @param description string to descibe the todo item
  void addTodo(String description) {
    activeTodoList.add(TodoModel(activeTodoList.length, description));
    notifyListeners();
  }

  /// Deactivate an item in the todo list
  /// @param id identifier of the item to deactivate
  void clearTodo(int id) {
    String description = activeTodoList[id].description;
    inactiveTodoList.add(TodoModel(inactiveTodoList.length, description));
    activeTodoList.removeAt(id);
    _updateTodoList(activeTodoList);
    notifyListeners();
  }

  void restoreTodo(int id) {
    String description = inactiveTodoList[id].description;
    activeTodoList.add(TodoModel(activeTodoList.length, description));
    inactiveTodoList.removeAt(id);
    _updateTodoList(inactiveTodoList);
    notifyListeners();
  }

  /// Remove a todo item from the todo list
  /// @param id identifier of the item to remove
  void removeTodo(int id) {
    inactiveTodoList.removeAt(id);
    _updateTodoList(inactiveTodoList);
    notifyListeners();
  }

  /// Updates ID values of todo items, must be called after any operation that
  /// edits the activeTodoList out of order
  void _updateTodoList(List<TodoModel> todoList) {
    for (var i = 0; i < todoList.length; i++) {
      todoList[i].id = i;
    }
  }
}
