import 'package:flutter/material.dart';

import 'package:todo_app/src/screens/app_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const ColorScheme colorSchemeLight = ColorScheme(
    primary: Color(0xFF4099C9),
    onPrimary: Color(0xFF081116),
    secondary: Color(0xFF87C3E3),
    onSecondary: Color(0xFF081116),
    tertiary: Color(0xFF5CB2E0),
    onTertiary: Color(0xFF081116),
    background: Color(0xFFF8FBFC),
    onBackground: Color(0xFF000000),
    error: Color(0xFF000000),
    onError: Color(0xFFFF0000),
    surface: Color(0xFFFFFFFF),
    onSurface: Color(0xFFFFFFFF),
    brightness: Brightness.light,
  );
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF4099C9)),
        useMaterial3: true,
      ),
      darkTheme: ThemeData(
        colorScheme: const ColorScheme.dark(),
        useMaterial3: true,
      ),
      themeMode: ThemeMode.light,
      home: const AppScreen(),
    );
  }
}
